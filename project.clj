(defproject async-cljs "0.1.0-SNAPSHOT"
  :description "FIXME: write this!"
  :url "http://example.com/FIXME"

  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.clojure/clojurescript "0.0-2173"]
                 [org.clojure/core.async "0.1.303.0-886421-alpha"]
                 [prismatic/dommy "0.1.2"]
                 [garden "1.1.7"]
                 [ring "1.3.0"]
                 [hiccup "1.0.5"]]

  :plugins [[lein-cljsbuild "1.0.2"]]

  :source-paths ["src/clj"]

  :cljsbuild {
    :builds [{:id "async-cljs"
              :source-paths ["src"]
              :compiler {
                :output-to "resources/public/js/cljs.js"
                :output-dir "resources/public/js"
                :optimizations :advanced
                :source-map "resources/public/js/cljs.js.map"
                         }}]})
