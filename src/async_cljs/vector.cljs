(ns async-cljs.vector)

(defrecord vector2 [x y])

(defn make
  ([]
     (vector2. 0 0))
  ([x y]
     (vector2. x y)))

(defn add [a b]
  (make
    (+ (:x a) (:x b))
    (+ (:y a) (:y b))))

(defn sub [a b]
  (make
    (- (:x a) (:x b))
    (- (:y a) (:y b))))

(defn mult [vec constant]
  (make
    (* (:x vec) constant)
    (* (:y vec) constant)))

(defn div [vec constant]
  (make
    (/ (:x vec) constant)
    (/ (:y vec) constant)))

(defn length-squared [vec]
  (+
    (* (:x vec) (:x vec))
    (* (:y vec) (:y vec))))

(defn length [vec]
  (Math/sqrt (length-squared vec)))

(defn unit? [vec]
  (= 1.0 (length vec)))

(defn unit [vec]
  (div vec (length vec)))

(defn iszero? [vec]
  (and
    (zero? (:x vec))
    (zero? (:y vec))))

(defn dotprod [v1 v2]
  (+
    (* (:x v1) (:x v2))
    (* (:y v1) (:y v2))))

(defn rotate [vec ang]
  (let [cos (Math/cos ang)
        sin (Math/sin ang)
        x (:x vec)
        y (:y vec)]
    (vector2. (- (* cos x) (* sin y))
              (+ (* sin x) (* cos y)))))
