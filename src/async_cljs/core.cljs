(ns async-cljs.core
  (:require [cljs.core.async :refer [put! chan <!]]
            [dommy.utils :as utils]
            [dommy.core :as dommy]
            [clojure.browser.repl :as repl]
            [async-cljs.vector :as vector])
  (:require-macros [cljs.core.async.macros :refer [go]]
                   [async-cljs.macros :refer [breakpoint]])
  (:use-macros [dommy.macros :only [node sel sel1]])
  (:import [goog.net Jsonp]
           [goog Uri]))


(enable-console-print!)
(.log js/console "Example async clojurescript app")
(println "...")

(def wiki-search-url
  "http://en.wikipedia.org/w/api.php?action=opensearch&format=json&search=")

(defn listen [el type]
  (let [out (chan)]
    (dommy/listen! el type
                   ; the event callback for eventSource el, eventType type
                   (fn [e]
                     ; put it on the channel
                     (put! out e)))
    ; return the channel
    out))

(defn jsonp [uri]
  (let [out (chan)
        ;; http request for json
        req (Jsonp. (Uri. uri))]
    ;; senf the request
    (.send req nil
           ;; callback function for response
           (fn [res] (put! out res)))
    ;; return the channel
    out))

(defn query-url [q]
  (str wiki-search-url q))

(defn user-query []
  (.-value (sel1 :#query)))

(defn render-query [results]
  (node
   [:ul
    (for [result results]
      [:li result])]))

(defn init-search []
  (let [clicks (listen (sel1 :#search) :click)
        results-view (sel1 :#results)]
    (go (while true
          (<! clicks)
          (let [[_ results] (<! (jsonp (query-url (user-query))))
                rendered (render-query results)]
            (.log js/console rendered)
            (dommy/set-html! results-view (dommy/html rendered)))))))

(init-search)

(js/setTimeout (fn [] (.log js/console "tick")) 5000)

;;
;; flying ship
;;
(dommy/append! (sel1 :#container)
               (node [:div
                      {:class "sprite" :id "monk"}]))

; (println (dommy/get-px (sel1 :#container)))

(def width 1000)
(def height 800)

(defn- wrap [pos]
  (vector/make (mod (:x pos) width) (mod (:y pos) height)))

(defn- setpos [pos]
  (do
    (dommy/set-px! (sel1 :#monk) :margin-left (:x pos))
    (dommy/set-px! (sel1 :#monk) :margin-top (:y pos))))

(defn- setrot [ang]
  (dommy/set-style! (sel1 :#monk) :-webkit-transform (str "rotate(" ang "rad)")))

(def pos (atom (vector/make 100 100)))
(def heading (atom 0))

(defn- update! [pos heading]
  (swap! pos vector/add (vector/rotate (vector/make 0 -1) @heading))
  (setpos (wrap @pos))
  (setrot @heading))

(defn- randint [min max]
  (+ (* (- max min) (Math/random)) min))

(let [tchan (chan)]
  (js/setInterval
   (fn []
     (put! tchan true))
   (/ 1 60)
   )
  (go
   (while true
     (let [dir (/ (- (* (rand) 2) 1) 100)]
       (dotimes [n (randint 100 300)]
         (swap! heading + dir)
         (update! pos heading)
         (<! tchan))))))
