(ns async-cljs.macros)

;; breakpoint macro from
;; https://github.com/shaunlebron/How-To-Debug-CLJS
(defmacro breakpoint []
  '(do (js* "null; debugger;")
       nil)) ; (prevent "return debugger;" in compiled javascript)
