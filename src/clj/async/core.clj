(ns async.core
  (:require [ring.adapter.jetty :refer [run-jetty]]
            [ring.middleware.resource :as resource]
            [ring.util.response :as response]
            [garden.core :refer [css]]
            [hiccup.core :refer [html]]))

(defn router [request]
  (let [uri (:uri request)]
    (cond
     (= "/" uri)
     {:status 200
      :headers {"Content-Type" "text/html"}
      :body (html
             [:html
              [:head
               [:title "Index Page"]
               [:link {:rel "stylesheet" :type "text/css" :href "/css/sprite.css" }]]
              [:body
               ;; search box
               [:input#query {:type "text"}]
               [:button#search "Search"]
               [:p#results]

               ;; container for sprite
               [:div#container]

               ;; scripts
               [:script {:src "js/cljs.js" :type "text/javascript"}]]])}

     (= "/css/sprite.css" uri)
     {:status 200
      :headers {"Content-Type" "text/css"}
      :body (css [:.sprite
                  {
                   :background-image "url('/playerShip1_blue.png')"
                   :background-color "transparent"
                   :background-repeat "no-repeat"
                   :display "block"
                   :height "64px"
                   :width "64px"
                   :-webkit-transform "rotate(30deg)"
                   :margin-left "200px"
                   :margin-top "100px"
                   :background-position "0px 0"
                   :background-size "64px"}])})))

(defn -main [& args]
  (run-jetty
   (-> router
       (resource/wrap-resource "public")) {:port 8000}))
