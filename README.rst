Example of asynchronous clojurescript
=====================================

A test project using clojurescript to combine async.core go blocks to
asynchronously drive the page using dommy to mutate the DOM.

Setup
-----

  * git clone
  * run `lein cljsbuild auto async-cljs`
  * load index.html into browser

Run Webserver
-------------

  * run `lein run -m async.core`

Graphics
--------

Monk sprite from '700 Sprites' by Phillip Lenssen

http://opengameart.org/content/700-sprites
